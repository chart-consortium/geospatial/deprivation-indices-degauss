# Deprivation Indices Degauss

A repo to build a container for running [Degauss deprivation indices](https://degauss.org/dep_index/) to obtain deprivation index values for geocoded locations. Note that the deprivation index is from Brokamp et al 2019 ([details here](https://geomarker.io/dep_index/)). The container is bootstrapped from the ['deprivation index' image](https://github.com/degauss-org/dep_index), can run RStudio Server and has additional tools including tidyverse and the 'sf' R package.

## Building

- A new container build will only be initiated by a modification to the Singularity.def file
- The built container file will only be pushed to the Duke OIT registry when a commit is tagged (ideally following the current version naming convention)
    + Note that to push, the container must be re-built first.

## Pulling

To pull the latest version of the container file via Singularity, use the following command where "/path/to/container/directory" is the directory where the container will be pulled to:

```
singularity pull --force --dir /path/to/container/directory oras://gitlab-registry.oit.duke.edu/chart-consortium/geospatial/deprivation-indices-degauss/deprivation-indices-degauss:latest
```

Alternatively, the latest container version can be pulled using wget (to the current directory by default):

```
wget --no-check-certificate https://research-singularity-registry-public.oit.duke.edu/chart-consortium/deprivation-indices-degauss.sif
```

## Running

The container can be run using something like the following, where "path to geocoded addresses" is the path to CSV file with geocoded addresses:

```
export ADDRESS_FILE="path to geocoded addreses"
singularity run deprivation-indices-degauss.sif ${ADDRESS_FILE}
```

## Notes



